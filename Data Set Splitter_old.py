import sys
import os
import json
import sqlite3
import pandas
import time
import csv


DATA_DIR = "C:/Users/hedbn/kaggle/"
SPLITTED_DATA_DIR = "C:/Users/hedbn/kaggle/splitted/"

def myPrint(m):
    print(m)

def main():
    os.chdir(DATA_DIR)
    #split_train(10000)
    split_members()

def split_members():

    try:
        members_DB = sqlite3.connect(DATA_DIR + "members_DB")

        with open(DATA_DIR + "members.csv", 'r') as f:
            members_header = f.readline()


        ls = os.listdir(SPLITTED_DATA_DIR)
        for splitted_train_csv in ls:
            if (splitted_train_csv.startswith("train_")):
                train_csv = pandas.read_csv(SPLITTED_DATA_DIR + splitted_train_csv)
                create_member_file_according_to_training_file(splitted_train_csv,members_header,members_DB,train_csv)

    finally:
        members_DB.close()

def create_member_file_according_to_training_file(splitted_train_csv,members_header,members_DB,train_csv):
    splitted_members_csv_name = splitted_train_csv.replace("train_","members_")
    myPrint("Creating " +splitted_members_csv_name )

    missed_members = 0

    with open(SPLITTED_DATA_DIR + splitted_members_csv_name, 'w',newline="\n") as splitted_members_csv:
        splitted_members_csv.write(members_header)
        csv_out = csv.writer(splitted_members_csv)

        for index, train_row in train_csv.iterrows():
            cur = members_DB.cursor()
            sql_str = "select * from members_t where msno = '"+ train_row['msno']+"' ; "
            cur.execute(sql_str)
            row = cur.fetchone()
            if (row != None):
                csv_out.writerow(row)
            else :
                missed_members += 1
                myPrint("Missing " + train_row['msno'])

    myPrint("Didn't find " + str(missed_members) + " members")


def split_train(N):

    with open("C:/Users/hedbn/kaggle/train.csv") as f:
        lines = f.readlines()
        header = lines.pop(0)

        file_index = 0
        line_index = 0
        output_file = None


        for line in lines :
            if (line_index > N):
                line_index = 0
            if (line_index == 0):
                if (output_file):
                    output_file.close()
                file_index +=1
                output_file = open("C:/Users/hedbn/kaggle/splitted/train_" + str(file_index) +".csv","w")
                output_file.write(header)

            output_file.write(line)
            line_index +=1



if __name__ == "__main__":
    # execute only if run as a script
    main()