

import os
import pandas
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop
import numpy as np


import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop



DATA_DIR = "C:/Users/hedbn/kaggle/splitted/train_1/"

def main():

    helper_vec_dim = 10
    data_size = 35

    batch_size = 128
    num_classes = 2
    epochs = 20

    (x_train, y_train) = get_data_from_files("1",helper_vec_dim)
    (x_test, y_test) = get_data_from_files("2",helper_vec_dim)

    print(x_train.shape)
    print(x_test.shape)

    x_train = x_train.reshape(20002, data_size+helper_vec_dim)
    x_test = x_test.reshape(20002, data_size+helper_vec_dim)
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')

    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    model = Sequential()
    model.add(Dense(512, activation='relu', input_shape=(data_size+helper_vec_dim,)))
    model.add(Dropout(0.2))
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(num_classes, activation='softmax'))

    model.summary()

    model.compile(loss='categorical_crossentropy',
                  optimizer=RMSprop(),
                  metrics=['accuracy'])

    history = model.fit(x_train, y_train,
                        batch_size=batch_size,
                        epochs=epochs,
                        verbose=1,
                        validation_data=(x_test, y_test))
    score = model.evaluate(x_test, y_test, verbose=0)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])


def get_data_from_files(chunk_id,use_helper_vec):

    # train_f = np.genfromtxt('train_t_sorted0.csv.as_images', delimiter=',')
    train_f = np.genfromtxt('train_f_sorted'+chunk_id+'.csv.as_images', delimiter=',')
    train_f = np.delete(train_f, [0, 1], axis=1)
    train_f = np.delete(train_f, [0], axis=0)

    if (use_helper_vec):
        helper_vec = np.random.random_integers(0, 1, (train_f.shape[0],use_helper_vec))
        train_f = np.concatenate((train_f, helper_vec),1)

    y_f = np.zeros((train_f.shape[0]))



    # train_t = np.genfromtxt('train_t_sorted0.csv.as_images', delimiter=',')
    train_t = np.genfromtxt('train_t_sorted'+chunk_id+'.csv.as_images', delimiter=',')
    train_t = np.delete(train_t, [0, 1], axis=1)
    train_t = np.delete(train_t, [0], axis=0)

    if (use_helper_vec):
        helper_vec = np.random.random_integers(101, 102, (train_t.shape[0],use_helper_vec))
        train_t = np.concatenate((train_t, helper_vec),1)

    y_t = np.ones((train_t.shape[0]))

    train_x = np.concatenate((train_f, train_t))
    train_y = np.concatenate((y_f, y_t))
    return train_x, train_y


if __name__ == "__main__":
    os.chdir(DATA_DIR)
    main()