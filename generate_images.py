import os
import pandas
import datetime
import datetime
#
#
#		Spread	Size
# msno,
# is_churn
#
# Members
# msno
# city	0-25	255	8*8
# bd	15-80	255	8*8
# gender	default = 0, male=1, female=2	255	8*8
# registered_via	0-20	255	8*8
# registration_init_time	-
# expiration_date	c
# calc - Active_time_in_month	0-156	255	8*8
# min(expiration_date,28/2/2017) - max(registration_init_time,01/01/2004)
#
# transactions
# include last 5 which transaction_date < transaction (is_cancel = 1)
#
# msno
# payment_method_id	0-50	255	2*8
# payment_plan_days	0-500	255	2*8
# plan_list_price	0-2000	255	2*8
# actual_amount_paid	-
# is_auto_renew	0-1	255	2*8
# transaction_date
# membership_expire_date
# is_cancel
#
# user_logs	last 60 days, desc order
# msno
# date
# num_25	0-255
# num_50	0-255
# num_75	0-255
# num_985	0-255
# num_100	0-255
# num_unq	0-255
# total_secs	-
#

class DataToPixel_Transformer:
    def __init__(self, def_arr):
        self.def_arr = def_arr
        for col_def in self.def_arr:
            labels = col_def.get('labels')
            if (None != labels):
                values = labels.values()
                col_def['min']= min(values)
                col_def['max']= max(values)
        return

    def extract_columns(self):
        ret = []
        for col_def in self.def_arr:
            weight = col_def["weight"]
            name = col_def["name"]
            for i in range(1,weight + 1):
                ret.append(name + "_px" + str(i))
        return ret

    def extract_pixels(self,dict):
        ret = []
        for col_def in self.def_arr:
            name = col_def["name"]
            v = dict.get(name)
            if (None == v):
                supporting_cols = col_def.get('based_on')
                if (supporting_cols != None):
                    missing_supporting_col = False
                    for col_name in supporting_cols:
                        if (None == dict.get(col_name)):
                            missing_supporting_col = True
                    if (not missing_supporting_col):
                        calc_func = col_def.get('calc_func')
                        v = calc_func(dict)

            if (None == v):
                v = 0
            weight = col_def["weight"]
            spread = col_def["spread"]
            labels = col_def.get('labels')
            if (None != labels):
                v = labels.get(v)
                if (None == v):
                    v = labels.get('default__')

            min_v = col_def["min"] + 0.0
            max_v = col_def["max"]+ 0.0
            v = max(min_v,v)
            v = min(max_v, v)
            v = v - min_v
            v = round(spread/(max_v-min_v) * v)
            for i in range(1, weight + 1):
                ret.append(v)

        return ret


class DataImage:

    def __init__(self, case_line):
        self.case_line = case_line
        self.id = case_line.get("msno")
        self.series = pandas.Series(
            index = ['1id','2label'],
            data = [self.id,case_line.get("is_churn")]
        )

    def get_series(self):
        return self.series

    # Members
    # msno
    # city	0-25	255	8*8
    # bd	15-80	255	8*8
    # gender	default = 0, male=1, female=2	255	8*8
    # registered_via	0-20	255	8*8
    # registration_init_time	-
    # expiration_date	c
    # calc - Active_time_in_month	0-156	255	8*8
    # min(expiration_date,28/2/2017) - max(registration_init_time,01/01/2004)


    def add_details(self,details_df,details_index):

        details = []

        def calc_active_time_in_months(dict):
            init_time = datetime.datetime.strptime(str(dict.get("init_time")), '%Y%m%d').date()
            expiration_date = datetime.datetime.strptime(str(dict.get("expiration_date")), '%Y%m%d').date()
            delta = min(datetime.date(2017, 2, 28), expiration_date) - max(datetime.date(2004, 1, 1), init_time)
            in_months = round(delta.days / 30)
            return in_months


        details_def = DataToPixel_Transformer([
            {'name':'city',"min":0,"max":25,"spread":255,"weight":8},
            {'name': 'bd', "min": 15, "max": 80, "spread": 255, "weight": 8},
            {'name': 'gender', "labels": {'male':1,"female":2,"default__":0} , "spread": 255, "weight": 8},
            {'name': 'registered_via', "min": 0, "max": 20, "spread": 255, "weight": 8},
            {'name': 'active_time_in_months', "min": 0, "max": 156, "spread": 255, "weight": 2,
             'based_on': ['init_time', 'expiration_date'],
             'calc_func': calc_active_time_in_months}
        ])

        test = details_df.iloc[details_index].get("msno")
        while (details_index < details_df.shape[0] and details_df.iloc[details_index].get("msno") <= self.id):
            if (details_df.iloc[details_index].get("msno") == self.id):
                details = details_df.iloc[details_index]
            details_index += 1

        if (len(details) > 0):
            self.series = self.series.combine_first(pandas.Series(
                index= details_def.extract_columns(),
                data=details_def.extract_pixels(details)
            ))



        return details_index

DATA_DIR = "C:/Users/hedbn/kaggle/splitted/train_1/"
def combine_files_set_to_image_data(cases_filename, details_filename, transactions_file, usage_logs_file):
    output_file_name = cases_filename + ".as_images"
    cases_file = pandas.read_csv(cases_filename)
    details_df = pandas.read_csv(details_filename)
    details_index = 0
    image_df = pandas.DataFrame()

    for index, case_line in cases_file.iterrows():
        img = DataImage(case_line)
        details_index = img.add_details(details_df,details_index)
        image_df = image_df.append(img.get_series(),ignore_index=True)
        # if (index>2): break
    image_df.to_csv(output_file_name)


def combine_files_set_to_image_data_(N):
    combine_files_set_to_image_data('train_t_sorted'+N+'.csv','members_t_sorted'+N+'.csv','transactions_t_sorted'+N+'.csv','user_logs_t_sorted'+N+'.csv')
    combine_files_set_to_image_data('train_f_sorted'+N+'.csv','members_f_sorted'+N+'.csv','transactions_f_sorted'+N+'.csv','user_logs_f_sorted'+N+'.csv')


if __name__ == "__main__":
    os.chdir(DATA_DIR)

    # combine_files_set_to_image_data_("3")
    combine_files_set_to_image_data_("4")




    # main('train_t_sorted0.csv','members_t_sorted1.csv','transactions_t_sorted1.csv','user_logs_t_sorted1.csv')