import sys
import os
import json
import sqlite3
import pandas
import time


def myPrint(m):
    print(m)


def main():
    os.chdir("C:/Users/hedbn/kaggle")
    upload_file_to_db("members")


def upload_file_to_db(entity_name):

        chunksize = 1000 * 1000

        try:
            db_name = entity_name + "_DB"
            if (os.path.exists(db_name) ):
                exit("File " + db_name + " already exists")

            conn = sqlite3.connect(db_name)

            csv_file_name = entity_name + ".csv"
            table_name = entity_name + "_t"  # verifying were not stepping on some reserved word
            lineCount = 0
            myPrint("reading " + csv_file_name + " into " + table_name + " table")
            reader = pandas.read_csv(csv_file_name, chunksize=chunksize)
            for chunk in reader:
                myPrint(
                        "read " + str(chunksize) + " lines => writing it to DB. (Already " + str(lineCount) + ")")
                lineCount += chunksize
                chunk.to_sql(table_name, conn, if_exists='append', index=False)
        finally:
            conn.close()


if __name__ == "__main__":
    # execute only if run as a script
    main()