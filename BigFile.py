import pandas
import os
import shutil
import time


def myPrint(m):
    print(m)


class BigFile:

    path = ''
    sorted_path = ''
    tmp_last_measured = time.time()


    def __init__(self,path,sorted_path = None):
        self.path = path
        if (sorted_path == None):
            self.sorted_path = path + ".sorted"
        else :
            self.sorted_path = sorted_path


    sorted_file = None
    current_line = ''
    last_position = 0

    def startIterating(self,skip_header = False):
        self.sorted_file = open(self.sorted_path)
        self.sorted_file.seek(0,2)
        self.last_position = self.sorted_file.tell()
        self.sorted_file.seek(0, 0)
        if (skip_header):
            self.advanceLine()
            self.advanceLine()

    def advanceLine(self):
        if (self.sorted_file.tell() == self.last_position or self.last_position == -1):
            self.current_line = ''
            self.last_position = -1
            return False
        else:
            self.current_line = self.sorted_file.readline()
            return True

    def getCurrentLine(self):
        if (self.sorted_file.tell() == 0):
            self.advanceLine()
        return self.current_line

    def isLastLine(self):
        return (self.last_position == -1 or self.last_position == 0)






    def getElapsed(self):
        now = time.time()
        ret = str(now - self.tmp_last_measured)
        self.tmp_last_measured = now
        return ret

    def getHeader(self):
        header = ''
        with open(self.path) as infile:
            for line in infile:
                header = line
                break
        return header

    def sort(self):

        if (os.path.exists(self.sorted_path)):
            return

        tmp_dir_name = "BigFile_tmp_dir_for_sorting_" + pandas.datetime.now().strftime('%Y_%m_%d_%H_%M_%S') + "/"
        os.mkdir(tmp_dir_name)
        splited_files = {}
        i=0
        csv_header = ''
        with open(self.path) as infile:
            for line in infile:
                i += 1
                if (i == 1):
                    csv_header = line
                    continue
                tmp_sub_dir_name = tmp_dir_name + str(ord(line[0])).zfill(3) + "_" + str(ord(line[1])).zfill(3)
                tmp_filename = tmp_sub_dir_name + "/data.csv"
                if (tmp_filename not in splited_files):
                    os.mkdir(tmp_sub_dir_name)
                    f = open(tmp_filename,"w")
                    splited_files[tmp_filename] = f
                splited_files[tmp_filename].write(line)
                if(i%(1000*1000*10) == 0):
                    myPrint("Processed " + str(i) + " lines, took " + self.getElapsed() + " seconds")
                # if (i> (1000*1000) ):
                #     break

        for file in splited_files.values():
            file.close()

        ordered_file_list = []
        # Finished going over the lines
        for filename in splited_files.keys():
            ordered_file_list.append(filename)
            entity_list = []
            with open(filename,"r") as file:
                entity_list = file.readlines()
                entity_list.sort()
            with open(filename + ".sorted","w") as file:
                for line in entity_list:
                    file.write(line)
            myPrint("Sorted " + filename + ", took " + self.getElapsed() + " seconds")

        ordered_file_list.sort()

        with open(self.sorted_path,'w') as sorted_unified_file:
            sorted_unified_file.write(csv_header)
            for filename in ordered_file_list:
                with open(filename + ".sorted") as sorted_file_chunk:
                    shutil.copyfileobj(sorted_file_chunk, sorted_unified_file)
                myPrint("Appended " + filename + ", took " + self.getElapsed() + " seconds")







