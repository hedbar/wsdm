import BigFile
import pandas
import os
import generate_images
import datetime

tmp_dir_name = "test_dir_" + pandas.datetime.now().strftime('%Y_%m_%d_%H_%M_%S') + "/"
DATA_DIR = "C:/Users/hedbn/kaggle/"
os.mkdir(DATA_DIR + tmp_dir_name)
os.chdir(DATA_DIR + tmp_dir_name)


def myPrint(m):
    # print(m)
    return


def test0():
    myPrint("test0")
    filename =  'test_file0.txt'
    with open(filename,"w") as test_file:
        do = None

    test_file = BigFile.BigFile(filename,filename)
    test_file.startIterating()

    while (not test_file.isLastLine()):
        myPrint("line:" + test_file.getCurrentLine())
        test_file.advanceLine()




def test1():
    myPrint("test1")
    filename =  'test_file1.txt'
    with open(filename,"w") as test_file:
        test_file.write("first line")

    test_file = BigFile.BigFile(filename,filename)
    test_file.startIterating()

    while (not test_file.isLastLine()):
        myPrint("line:" + test_file.getCurrentLine())
        test_file.advanceLine()



def test2():
    myPrint("test2")
    filename =  'test_file2.txt'
    with open(filename,"w") as test_file:
        test_file.write("first line")
        test_file.write("\nsecond line")

    test_file = BigFile.BigFile(filename,filename)
    test_file.startIterating()

    while (not test_file.isLastLine()):
        myPrint("line:" + test_file.getCurrentLine())
        test_file.advanceLine()




def test0_1():
    myPrint("test0_1")
    filename =  'test_file0.txt'
    with open(filename,"w") as test_file:
        do = None

    test_file = BigFile.BigFile(filename,filename)
    test_file.startIterating()

    while (test_file.advanceLine()):
        myPrint("line:" + test_file.getCurrentLine())

    if (not test_file.isLastLine()):
        print ("Error - finished before last line")



def test1_1():
    myPrint("test1_1")
    filename =  'test_file1.txt'
    with open(filename,"w") as test_file:
        test_file.write("first line")

    test_file = BigFile.BigFile(filename,filename)
    test_file.startIterating()

    while (test_file.advanceLine()):
        myPrint("line:" + test_file.getCurrentLine())

    if (not test_file.isLastLine()):
        print ("Error - finished before last line")


def test2_1():
    myPrint("test2_1")
    filename =  'test_file2.txt'
    with open(filename,"w") as test_file:
        test_file.write("first line")
        test_file.write("\nsecond line")

    test_file = BigFile.BigFile(filename,filename)
    test_file.startIterating()

    while (test_file.advanceLine()):
        myPrint("line:" + test_file.getCurrentLine())

    if (not test_file.isLastLine()):
        print ("Error - finished before last line")


def test1_DataToPixel_Transformer():

    tf = generate_images.DataToPixel_Transformer([
            {'name':'city',"min":0,"max":25,"spread":255,"weight":3}
        ])

    index_res = tf.extract_columns()

    assert index_res==['city_px1','city_px2','city_px3'] , index_res

    data_res = tf.extract_pixels({'city':7.0})
    assert data_res == [71,71,71] , data_res

    data_res = tf.extract_pixels({'city':-7.0})
    assert data_res == [0,0,0] , data_res

    data_res = tf.extract_pixels({'city':99.0})
    assert data_res == [255,255,255] , data_res #max after spread


def test2_DataToPixel_Transformer():

    tf = generate_images.DataToPixel_Transformer([
            {'name':'city',"min":0,"max":25,"spread":255,"weight":3},
            {'name': 'bd', "min": 25, "max": 50, "spread": 255, "weight": 3}
        ])

    index_res = tf.extract_columns()

    assert index_res==['city_px1','city_px2','city_px3','bd_px1','bd_px2','bd_px3'] , index_res

    data_res = tf.extract_pixels({'city':7.0,'bd':6})
    assert data_res == [71,71,71,0,0,0] , data_res

    data_res = tf.extract_pixels({'city':-7.0,'dd':0})
    assert data_res == [0,0,0,0,0,0] , data_res

    data_res = tf.extract_pixels({'city':99.0,'bd':-5,'dd':0})
    assert data_res == [255,255,255,0,0,0] , data_res #max after spread


def test3_DataToPixel_Transformer():

    tf = generate_images.DataToPixel_Transformer([
        {'name': 'city', "min": 0, "max": 25, "spread": 255, "weight": 1},
        {'name': 'bd', "min": 15, "max": 80, "spread": 255, "weight": 1},
        {'name': 'gender', "labels": {'male': 1, "female": 2, "default__": 0}, "spread": 255, "weight": 2},
    ])

    data_res = tf.extract_pixels({'gender':'male'})
    assert data_res == [0,0,128,128] , data_res

    data_res = tf.extract_pixels({'gender':'female'})
    assert data_res == [0,0,255,255] , data_res

    data_res = tf.extract_pixels({'gender':''})
    assert data_res == [0,0,0,0] , data_res


    data_res = tf.extract_pixels({'gender':''})
    assert data_res == [0,0,0,0] , data_res



def test4_DataToPixel_Transformer():

    # min(expiration_date,28/2/2017) - max(registration_init_time,01/01/2004)
    def calc_active_time_in_months(dict):
        init_time = datetime.datetime.strptime(str(dict.get("init_time")), '%Y%m%d').date()
        expiration_date = datetime.datetime.strptime(str(dict.get("expiration_date")), '%Y%m%d').date()
        delta = min(datetime.date(2017, 2, 28),expiration_date) -  max(datetime.date(2004, 1, 1),init_time)
        in_months = round( delta.days / 30)
        return in_months


    tf = generate_images.DataToPixel_Transformer([
        {'name': 'registered_via', "min": 0, "max": 20, "spread": 255, "weight": 1},
        {'name':'active_time_in_months',"min": 0, "max": 156, "spread": 255, "weight": 2,'based_on':['init_time','expiration_date'],
         'calc_func': calc_active_time_in_months }
    ])

    data_res = tf.extract_pixels({'init_time':20161115,'expiration_date':20170216})
    assert data_res == [0,5, 5], data_res




if __name__ == "__main__":
    # execute only if run as a script
    test0()
    myPrint("\n")
    test1()
    myPrint("\n")
    test2()
    myPrint("\n")

    myPrint("\n------------- New iteration style:\n")

    test0_1()
    myPrint("\n")
    test1_1()
    myPrint("\n")
    test2_1()
    myPrint("\n")


    myPrint("\n------------- Testing DataToPixel_Transformer:\n")

    test1_DataToPixel_Transformer()
    test2_DataToPixel_Transformer()
    test3_DataToPixel_Transformer()
    test4_DataToPixel_Transformer()
    print("\n------------- Finished well\n")
