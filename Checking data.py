import sys
import os
import json
import sqlite3
import pandas
import time


DB_FILE_NAME = "DB1"

INSTRUCTIONS_JSON = "instructions.json"

def myPrint(m):
    print(m)


def main():

    try:
        if (len(sys.argv) < 2):
            myPrint("Usage: Directory with list of CSVs or instructions.json file")
            exit()

        os.chdir(sys.argv[1])

        ls = os.listdir(".")
        if (INSTRUCTIONS_JSON not in ls):
            myPrint("Didn't find " + INSTRUCTIONS_JSON + " creating new")
            instructions = {}
        else:
            instructions_file = open(INSTRUCTIONS_JSON, 'r+')
            instructions=json.load(instructions_file)

            steps_to_perform = ['step1_files_to_deal_with','step2_files_processed','step3_upload_csv_to_db']
            stepInd = 0
            for step in steps_to_perform:
                stepInd+=1
                if (step not in instructions):
                    myPrint("Performing " + step)
                    start_time = time.time()
                    eval(step)(instructions)
                    myPrint("took " + str(time.time() - start_time) + " seconds")
                    return # performing one step at a time
                else:
                    print("found " + step + ", skipping to next step")


    except:
        raise

def step3_upload_csv_to_db(instructions):
    files = instructions['step2_files_processed']
    chunksize = 1000*1000

    try:
        conn = sqlite3.connect(DB_FILE_NAME)
        tables = get_tables(conn)

        for fileObj in files:
            csv_file_name = fileObj["name"]
            table_name = csv_file_name.rstrip(".csv") + "_t" #verifying were not stepping on some reserved word
            if (table_name not in tables): # and table_name == 'train_t'):
                lineCount = 0
                myPrint("reading " + csv_file_name + " into " + table_name + " table")
                reader = pandas.read_csv(csv_file_name,chunksize=chunksize)
                for chunk in reader:
                    myPrint("read "+str(chunksize)+" lines => writing it to DB. (Already " + str(lineCount) + ")")
                    lineCount += chunksize
                    chunk.to_sql(table_name, conn, if_exists='append', index=False)


        tables = get_tables(conn)
        jsonObj = []
        for table_name in tables:
            myPrint("writing obj for table " + table_name)
            table = {}
            table["name"] = table_name
            table["lineCount"] = getLineCount(conn,table_name)
            jsonObj.append(table)

        dump_to_file(instructions, sys._getframe().f_code.co_name, jsonObj)





    finally:
        conn.close()



def getLineCount(conn,table_name):
    cur = conn.cursor()
    cur.execute("SELECT count(1) FROM "+ table_name +";")
    return cur.fetchone()[0]


def get_tables(conn):
    tables = []
    cur = conn.cursor()
    cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
    rows = cur.fetchall()
    for row in rows:
        tables.append(row[0])
    return tables










def step1_files_to_deal_with(instructions):
    ls = os.listdir(".")
    myPrint(
        "Processing dir contents. Go over the created file and delete redundant files")
    files_to_deal_with = []
    for file_name in ls:
        if (file_name.endswith(".csv")):
            files_to_deal_with.append(file_name)

    dump_to_file(instructions, sys._getframe().f_code.co_name, files_to_deal_with)


def dump_to_file(instructions, element_name,element):
    instructions[element_name] = element
    json.dump(instructions, open(INSTRUCTIONS_JSON, 'w'), indent=True)


def step2_files_processed(instructions):

    files_to_deal_with = instructions['step1_files_to_deal_with']

    files = []
    for file_name in files_to_deal_with:
        reader = open(file_name,'r')
        first_line = reader.readline().rstrip('\n')
        myPrint("\n"+file_name + " columns:\n" + first_line)
        file = {"name":file_name , "columns":first_line}
        files.append(file)


    dump_to_file(instructions, sys._getframe().f_code.co_name, files)

    myPrint("\n\nVerify that you want to examine all the columns")




#
# cwd = os.getcwd()
#     print ("Number of arguments: ", len(sys.argv))
#
# print ("This is the name of the script: ", sys.argv[0])
# print ("Number of arguments: ", len(sys.argv))
# print (cwd)


# array = [] #declaring a list with name '**array**'
# with open(PATH,'r') as reader :
#     for line in reader :
#         array.append(line)

if __name__ == "__main__":
    # execute only if run as a script
    main()