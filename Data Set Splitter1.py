import sys
import os
import json
import sqlite3
import pandas
import time
import csv
import re
from BigFile import BigFile


DATA_DIR = "C:/Users/hedbn/kaggle/"
SPLITTED_DATA_DIR = "C:/Users/hedbn/kaggle/splitted/"

def myPrint(m):
    print(m)

def main():
    os.chdir(DATA_DIR)
    #split_train()
    # split_details_according_to_train("members")

    # split_details_according_to_train("transactions_test")
    # split_details_according_to_train_big_file("transactions_test_big_file")

    # split_details_according_to_train("transactions")
    # split_details_according_to_train_big_file("user_logs")


def split_details_according_to_train_big_file(entity_name):

    entity_file = BigFile(entity_name + ".csv")
    entity_csv_header = entity_file.getHeader()
    entity_file.sort()

    # prefixes = ['train_f_sorted','train_t_sorted']
    prefixes = ['train_t_sorted']

    for prefix in prefixes:

        entity_file.startIterating(skip_header=True)

        for i in range(1,10000):
            split_train_filename =  prefix + str(i) + ".csv"
            if (os.path.exists(SPLITTED_DATA_DIR + split_train_filename)):
                myPrint("Handling " + split_train_filename)
                collect_cases_according_to_splitted_train_big_file(split_train_filename,entity_name,entity_file,entity_csv_header)
            else :
                myPrint("Not finding " + split_train_filename)
                break
            # if (i>0):break



def collect_cases_according_to_splitted_train_big_file(split_train_filename, entity_name, entity_file, entity_csv_header):

    with open(SPLITTED_DATA_DIR + split_train_filename,"r") as train_file:
        train_lines = train_file.readlines()
        train_lines.pop(0)
        split_entities_filename = split_train_filename.replace("train",entity_name)
        with open(SPLITTED_DATA_DIR + split_entities_filename, "w") as splitted_entity_file:
            splitted_entity_file.write(entity_csv_header)
            pattern = re.compile("(.*)\,")
            for train_line in train_lines:
                msno = pattern.search(train_line)[1]

                while (not entity_file.isLastLine() and entity_file.getCurrentLine() < msno):
                    entity_file.advanceLine()

                if (entity_file.isLastLine()):
                    break; # Finished iterating the members file

                while (entity_file.getCurrentLine().startswith(msno)):
                    splitted_entity_file.write(entity_file.getCurrentLine())
                    entity_file.advanceLine()
    return





def split_details_according_to_train(entity_name):


    filename = entity_name + ".csv"
    sorted_filename = filename + ".sorted"
    if (os.path.exists(sorted_filename)):
        with open(sorted_filename) as entity_file:
            entity_list = entity_file.readlines()
            entity_csv_header = entity_list.pop(0)
    else:
        with open(filename) as entity_file:
            entity_list = entity_file.readlines()
            entity_csv_header = entity_list.pop(0)
            entity_list.sort()
            with open(sorted_filename,"w") as sorted_entity_file:
                sorted_entity_file.write(entity_csv_header)
                sorted_entity_file.writelines(entity_list)

    prefixes = ['train_f_sorted','train_t_sorted']

    for prefix in prefixes:
        list_index = 0
        for i in range(1,10000):
            split_train_filename =  prefix + str(i) + ".csv"
            if (os.path.exists(SPLITTED_DATA_DIR + split_train_filename)):
                myPrint("Handling " + split_train_filename)
                list_index = collect_cases_according_to_splitted_train(split_train_filename,entity_name,entity_list,list_index,entity_csv_header)
            else :
                myPrint("Not finding " + split_train_filename)
                break
            # if (i>0):break


def collect_cases_according_to_splitted_train(split_train_filename, entity_name, entity_list, list_index, entity_csv_header):

    with open(SPLITTED_DATA_DIR + split_train_filename,"r") as train_file:
        train_lines = train_file.readlines()
        train_lines.pop(0)
        split_entities_filename = split_train_filename.replace("train",entity_name)
        with open(SPLITTED_DATA_DIR + split_entities_filename, "w") as entity_file:
            entity_file.write(entity_csv_header)
            pattern = re.compile("(.*)\,")
            for train_line in train_lines:
                msno = pattern.search(train_line)[1]
                while (list_index < len(entity_list) and entity_list[list_index] < msno):
                    list_index += 1
                if (list_index == len(entity_list)):
                    break; # Finished iterating the members file
                while (entity_list[list_index].startswith(msno)):
                        entity_file.write(entity_list[list_index])
                        list_index += 1

    return list_index


def sort_train():

    if (os.path.exists(SPLITTED_DATA_DIR + "train_t_sorted.csv")):
        myPrint("Already sorted")
        return
    else:
        myPrint("Sorting train.csv")

    with open("train.csv") as train_file:
        lines = train_file.readlines()
        header = lines.pop(0)
        lines.sort()

        out_file_t = open(SPLITTED_DATA_DIR + "train_t_sorted.csv","w")
        out_file_t.write(header)
        out_file_f = open(SPLITTED_DATA_DIR + "train_f_sorted.csv", "w")
        out_file_f.write(header)

        for line in lines :
            strip_line = line.strip()
            if (strip_line.endswith("0")):
                out_file_f.write(line)
            elif (strip_line.endswith("1")):
                out_file_t.write(line)


def split_train():
    sort_train()
    split_file("train_t_sorted",10000)
    split_file("train_f_sorted",10000)


def split_file(file_name,N):

    with open(SPLITTED_DATA_DIR + file_name + ".csv") as f:
        lines = f.readlines()
        header = lines.pop(0)

        file_index = 0
        line_index = 0
        output_file = None


        for line in lines :
            if (line_index > N):
                line_index = 0
            if (line_index == 0):
                if (output_file):
                    output_file.close()
                file_index +=1
                output_file = open(SPLITTED_DATA_DIR + file_name + str(file_index) +".csv","w")
                output_file.write(header)

            output_file.write(line)
            line_index +=1




if __name__ == "__main__":
    # execute only if run as a script
    main()